package runner;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.Test;
//import org.junit.runner.RunWith;
//import io.cucumber.junit.Cucumber;
//import io.cucumber.junit.CucumberOptions;

	//@RunWith(Cucumber.class) 
	@Test
	@CucumberOptions(features ="src/test/resources/features",
	glue= {"MorbidityAPI","RecipesAPI","AppHooks"},
	plugin={"pretty",
			"rerun:target/failedrerun.txt"
			},
		
	//},"html:target/HtmlReports","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
   monochrome = true,
	dryRun = false,
	
			 publish = true
			 	 )
	public class MasterTestRunner extends io.cucumber.testng.AbstractTestNGCucumberTests 
	{

	}
